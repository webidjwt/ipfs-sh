#!/bin/bash
#
# Description:
# print IPFS node ID
#
# Authors:
# Jose G. Faisca <jose.faisca@gmail.com>
#

ID=$(ipfs id | grep ID | awk '{print $2}' | xargs)
ID=${ID%?}
echo "$ID"
