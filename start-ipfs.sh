#!/bin/bash
#
# Description:
# start IPFS daemon
#
# Authors:
# Jose G. Faisca <jose.faisca@gmail.com>
#

LOG_FILE="/tmp/ipfs.log"
ipfs daemon > $LOG_FILE &
sleep 2
pgrep ipfs